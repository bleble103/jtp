#include<iostream>
#include<string.h>
#include<unistd.h>

#include "bst.h"

using namespace std;

template <typename T> bool rowne(const T a, const T b, const T c){
    return (a==b) && (b==c);
}

bool rowne(const char *a, const char *b, const char *c){
    cout<<"Specjalizowana"<<endl;
    if(strcmp(a, b) == 0){
        return strcmp(b, c) == 0;
    }
    return false;
}

int main(){
    BSTTree<int> tree;
    try{
        tree.insert(5);
        tree.insert(1);
        tree.insert(2);
        tree.insert(6);
        //tree.insert(6);
        tree.insert(3);
    }catch(MyException &ex){
        cout<<"Exception: "<<ex.what()<<endl;
    }

    cout<<tree;

    return 0;
}
