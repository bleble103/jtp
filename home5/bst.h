#pragma once
#include<iostream>
#include<functional>
#include "myexception.h"

using namespace std;

template <typename T> class BSTTree;

template <typename T> class BSTNode{
    T value;
    BSTNode <T> *left;
    BSTNode <T> *right;

    public:
    BSTNode<T>(T value): value(value), left(NULL), right(NULL) {};
    ~BSTNode<T>();

    void insert(T);

    friend ostream& operator<<(ostream &os, const BSTNode<T> &node){
        return os<<node.value;
    }
    template <typename S> friend ostream& operator<<(ostream&, const BSTTree<S>&);
};

template <typename T> BSTNode<T>::~BSTNode<T>(){
    if(left != nullptr) delete left;
    if(right != nullptr) delete right;
    cout<<"Node deleted\n";
}

template <typename T> void BSTNode<T>::insert(T value){
    if(value == this->value){
        throw MyException("Element juz istnieje!");
    }
    BSTNode<T>* &next = (value > this->value) ? right : left;
    if(next == NULL){
        next = new BSTNode<T>(value);
    }else{
        next->insert(value);
    }
}

template <typename T> class BSTTree{
    BSTNode <T> *root;

    public:
    BSTTree<T>(): root(NULL) {};
    BSTTree<T>(const BSTTree<T>&) = delete;
    BSTTree<T> &operator=(const BSTTree<T>&) = delete;
    ~BSTTree<T>();
    void insert(T);

    template <typename S> friend ostream& operator<<(ostream&, const BSTTree<S>&);
};

template <typename T> BSTTree<T>::~BSTTree<T>(){
    if(root != nullptr){
        delete root;
    }
    cout<<"Tree deleted\n";
}

template <typename T> void BSTTree<T>::insert(T value){
    if(root != NULL){
        root->insert(value);
    }else{
        root = new BSTNode<T>(value);
    }
}

template <typename T> ostream& operator<<(ostream &os, const BSTTree<T> &tree){
    function<void(BSTNode<T>*)> aux = [&os, &aux](BSTNode<T> *node){
        if(node == nullptr){
            return;
        }
        aux(node->left);
        os<<*node<<endl;
        aux(node->right);
    };

    aux(tree.root);
    return os;

}