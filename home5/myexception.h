#pragma once
using namespace std;

class MyException : public exception{
    string what_message;
    public:
    MyException(string msg): what_message(msg){};
    const char* what(){
        return what_message.c_str();
    }
};