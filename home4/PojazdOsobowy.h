#pragma once
#include "Osoba.h"
#include "PojazdSilnikowy.h"
#include<sstream>
namespace MySpace{
	class PojazdOsobowy : public PojazdSilnikowy{
	protected:
		string marka;

	public:
		PojazdOsobowy() = delete;
		PojazdOsobowy(Osoba &wlasciciel, int przebieg, float pojemnosc, int moc_km, string marka): PojazdSilnikowy(wlasciciel, przebieg, pojemnosc, moc_km), marka(marka){
			cout<<"PojazdOsobowy contructor\n";
		}
		virtual ~PojazdOsobowy(){
			cout<<"PojazdOsobowy destructor\n";
		}
		string virtual opis() const{
			stringstream ss;
			wlasciciel.zapisz(ss);
			return "PojazdOsobowy " + marka +" osoby "+ss.str();
		}
		bool virtual zapisz(ostream &os) const{
			wlasciciel.zapisz(os);
			os<<" "<<pojemnosc<<" "<<moc_km<<" "<<marka;
			return true;
		}
		bool virtual wczytaj(istream &is){
			string imie, nazwisko;
			int rok_ur;
			is>>imie>>nazwisko>>rok_ur>>przebieg>>pojemnosc>>moc_km>>marka;
			wlasciciel = Osoba(imie, nazwisko, rok_ur);
			return true;
		}
	};
}