#pragma once
#include "Pojazd.h"
#include "Osoba.h"
#include<sstream>
namespace MySpace{
	class Rower : public Pojazd{
	protected:
		bool amatorski;

	public:
		Rower() = delete;
		Rower(Osoba &wlasciciel, int przebieg, bool amatorski): Pojazd(wlasciciel, przebieg), amatorski(amatorski){
			cout<<"Rower contructor\n";
		}
		virtual ~Rower(){
			cout<<"Rower destructor\n";
		}
		string virtual opis() const {
			stringstream ss;
			wlasciciel.zapisz(ss);
			return "Rower "+ ((amatorski) ? string("amatorski") : string("nie-amatorski")) + " osoby "+ss.str();
		}
		float virtual mocKW() const{
			return (amatorski) ? 0.3: 0.4;
		}
		bool virtual zapisz(ostream &os) const{
			wlasciciel.zapisz(os);
			os<<" "<<przebieg<<" "<<amatorski;
			return true;
		}
		bool virtual wczytaj(istream &is){
			string imie, nazwisko;
			int rok_ur;
			is>>imie>>nazwisko>>rok_ur>>przebieg>>amatorski;
			wlasciciel = Osoba(imie, nazwisko, rok_ur);
			return true;
		}
	};
}