#pragma once
#include "Pojazd.h"
#include "Osoba.h"
#include<sstream>
namespace MySpace{
	class PojazdSilnikowy : public Pojazd{
	protected:
		float pojemnosc;
		int moc_km;

	public:
		PojazdSilnikowy() = delete;
		PojazdSilnikowy(Osoba &wlasciciel, int przebieg, float pojemnosc, int moc_km): Pojazd(wlasciciel, przebieg), pojemnosc(pojemnosc), moc_km(moc_km){
			cout<<"PojazdSilnikowy contructor\n";
		}
		virtual ~PojazdSilnikowy(){
			cout<<"PojazdSilnikowy destructor\n";
		}
		string virtual opis() const{
			stringstream ss;
			wlasciciel.zapisz(ss);
			return "PojazdSilnikowy osoby "+ss.str();
		}
		float virtual mocKW() const{
			return 1/1.4*moc_km;
		}
		bool virtual zapisz(ostream &os) const{
			wlasciciel.zapisz(os);
			os<<" "<<pojemnosc<<" "<<moc_km;
			return true;
		}
		bool virtual wczytaj(istream &is){
			string imie, nazwisko;
			int rok_ur;
			is>>imie>>nazwisko>>rok_ur>>przebieg>>pojemnosc>>moc_km;
			wlasciciel = Osoba(imie, nazwisko, rok_ur);
			return true;
		}
	};
}