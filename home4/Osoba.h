#pragma once
#include<iostream>
using namespace std;
namespace MySpace{
	
	class Osoba{
		string imie;
		string nazwisko;
		int rok_ur;
	public:
		Osoba() = delete;
		Osoba(const string &imie, const string &nazwisko, int rok_ur): imie(imie), nazwisko(nazwisko), rok_ur(rok_ur) {};
		~Osoba(){
			cout<<"Osoba destructor\n";
		}
		bool zapisz(ostream &os) const {
			return &(os<<imie<<" "<<nazwisko<<" "<<rok_ur) != NULL;
		}
		bool wczytaj(istream &is){
			return &(is>>imie>>nazwisko>>rok_ur) != NULL;
		}
		void ustawImie(const string &im){
			this->imie = im;
		}
		string pobierzImie() const {
			return this->imie;
		}
		friend ostream& operator<<(ostream &os, const Osoba &o){
			return (o.zapisz(os))?os:os;
		}

	};
}