#include<iostream>
#include "Osoba.h"
#include "Pojazd.h"
#include "Rower.h"
#include "PojazdSilnikowy.h"
#include "PojazdOsobowy.h"



int main(){
	MySpace::Osoba o("a", "b", 1);
	MySpace::Pojazd *rowerek = new MySpace::Rower(o, 0, true);
	MySpace::Pojazd *silnik = new MySpace::PojazdSilnikowy(o, 0, 1.6, 106);
	MySpace::Pojazd *osob = new MySpace::PojazdOsobowy(o, 0, 1.6, 106, "Suzuki");
	o.ustawImie("Tomasz");

	cout<<endl;
	cout<<rowerek->opis()<<endl;
	cout<<"\tmocKW: "<<rowerek->mocKW()<<endl;
	rowerek->zapisz(cout);
	cout<<endl<<endl;

	cout<<silnik->opis()<<endl;
	cout<<"\tmocKW: "<<silnik->mocKW()<<endl;
	silnik->zapisz(cout);
	cout<<endl<<endl;
	
	cout<<osob->opis()<<endl;
	cout<<"\tmocKW: "<<osob->mocKW()<<endl;
	osob->zapisz(cout);
	cout<<endl<<endl;
	
	cout<<endl;

	delete rowerek;
	cout<<endl;

	delete silnik;
	cout<<endl;

	delete osob;
	cout<<endl;

	cout<<o<<endl;
	// MySpace::Rower r(o, 0, true);
	// cout<<r.opis()<<endl;
	//r.zapisz(cout);
	//r.wczytaj(cin);
	//r.zapisz(cout);
	//cout<<o.wczytaj(cin)<<endl;
	//o.zapisz(cout);
	//cout<<o<<endl;
	return 0;
}