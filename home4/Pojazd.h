#pragma once
#include "Osoba.h"
namespace MySpace{
	class Pojazd{
	protected:
		Osoba &wlasciciel;
		int przebieg;

	public:
		Pojazd() = delete;
		Pojazd(Osoba &wlasciciel, int przebieg) : wlasciciel(wlasciciel), przebieg(przebieg){
			cout<<"Pojazd constructor\n";
		}
		virtual ~Pojazd(){
			cout<<"Pojazd destructor\n";
		}
		string virtual opis() const = 0;
		float virtual mocKW() const = 0;
		bool virtual zapisz(ostream&) const = 0;
		bool virtual wczytaj(istream&) = 0;
	};
}