#pragma once
#include "Produkt.h"
#include "Data.h"
#include<sstream>
namespace MySpace{
	class Usluga : public Produkt{
	protected:
		double koszt_godziny;
		double ile_godzin;

	public:
		Usluga() = delete;
		Usluga(const string &nazwa, const Data &wyprodukowano, double koszt_godziny, double ile_godzin): Produkt(nazwa, wyprodukowano), koszt_godziny(koszt_godziny), ile_godzin(ile_godzin){
			cout<<"Usluga contructor\n";
		}
		virtual ~Usluga(){
			cout<<"Usluga destructor\n";
		}
		double virtual koszt() const{
			return koszt_godziny*ile_godzin;
		}
		string virtual opis() const; 
		bool virtual zapisz(ostream&) const;
		bool virtual wczytaj(istream&);
	};
}