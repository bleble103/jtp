#include "Towar.h"
namespace MySpace{
    string Towar::opis() const{
        stringstream ss;
        ss<<(termin_zdatnosci.pobierzRok());
        return "Towar zdatny do "+ss.str()+" roku\n";
    }

    
    bool Towar::zapisz(ostream &os) const{
        os<<nazwa<<" ";
        wyprodukowano.zapisz(os);
        os<<" "<<cena<<" "<<vat<<" ";
        termin_zdatnosci.zapisz(os);
        return true;
    }

    bool Towar::wczytaj(istream &is){
        int rok, miesiac, dzien;
        is>>nazwa>>rok>>miesiac>>dzien>>cena>>vat;
        wyprodukowano = Data(rok, miesiac, dzien);
        is>>rok>>miesiac>>dzien;
        termin_zdatnosci = Data(rok, miesiac, dzien);
        return true;
    }
}