#include<iostream>
#include "Data.h"
#include "Produkt.h"
#include "Usluga.h"
#include "Towar.h"
#include "TowarZKaucja.h"



int main(){
	MySpace::Data d1(1997,5,8);
	MySpace::Data d2(2019,5,8);
	MySpace::Produkt *obiekty[3];

	obiekty[0] = new MySpace::Usluga("nazwa", d1, 1.5, 5.0);
	obiekty[1] = new MySpace::Towar("nazwa", d1, 100, .23, d2);
	obiekty[2] = new MySpace::TowarZKaucja("nazwa", d1, 100, .23, d2, 121);


	cout<<endl;
	for(int i=0; i<3; i++){
		obiekty[i]->zapisz(cout);
		cout<<endl<<endl;
	}

	cout<<endl;
	for(int i=0; i<3; i++){
		cout<<obiekty[i]->opis();
		cout<<"\tkoszt: "<<obiekty[i]->koszt()<<endl<<endl;
	}

	cout<<"wczytaj towar: ";
	obiekty[1]->wczytaj(cin);
	cout<<obiekty[1]->opis();
	cout<<"\tkoszt: "<<obiekty[1]->koszt()<<endl<<endl;


	cout<<"Usuwanie uslugi:\n";
	delete obiekty[0];

	cout<<"\nUsuwanie towaru:\n";
	delete obiekty[1];

	cout<<"\nUsuwanie towaru z kaucja:\n";
	delete obiekty[2];
	cout<<endl;

	cout<<"wypisywanie daty d1:\n";
	cout<<d1<<endl;

	return 0;
}