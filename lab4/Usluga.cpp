#include "Usluga.h"
namespace MySpace{
    string Usluga::opis () const {
        stringstream ss;
        ss<<koszt();
        return "Usluga "+nazwa+" o koszcie "+ss.str()+"\n";
    }
    bool Usluga::zapisz(ostream &os) const{
        os<<nazwa<<" ";
        wyprodukowano.zapisz(os);
        os<<" "<<koszt_godziny<<" "<<ile_godzin;
        return true;
    }
    bool Usluga::wczytaj(istream &is){
        int rok, miesiac, dzien;
        is>>nazwa>>rok>>miesiac>>dzien>>koszt_godziny>>ile_godzin;
        wyprodukowano = Data(rok, miesiac, dzien);
        return true;
    }
}