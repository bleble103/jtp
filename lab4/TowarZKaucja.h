#pragma once
#include "Data.h"
#include "Towar.h"
#include<sstream>
namespace MySpace{
	class TowarZKaucja : public Towar{
	protected:
		double kaucja;

	public:
		TowarZKaucja() = delete;
		TowarZKaucja(const string &nazwa, const Data &wyprodukowano, double cena, double vat, const Data &termin_zdatnosci, double kaucja):Towar(nazwa, wyprodukowano, cena, vat, termin_zdatnosci), kaucja(kaucja){
			cout<<"TowarZKaucja contructor\n";
		}
		virtual ~TowarZKaucja(){
			cout<<"TowarZKaucja destructor\n";
		}
		string virtual opis() const;
		bool virtual zapisz(ostream&) const;
		bool virtual wczytaj(istream&);
	};
}