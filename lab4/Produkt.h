#pragma once
#include "Data.h"
namespace MySpace{
	class Produkt{
	protected:
		string nazwa;
		Data wyprodukowano;

	public:
		Produkt() = delete;
		Produkt(const string &nazwa, const Data &wyprodukowano) : nazwa(nazwa), wyprodukowano(wyprodukowano){
			cout<<"Produkt constructor\n";
		}
		virtual ~Produkt(){
			cout<<"Produkt destructor\n";
		}
		string virtual opis() const {
			return "Produkt o nazwie "+nazwa+"\n";
		};
		double virtual koszt() const = 0;
		bool virtual zapisz(ostream&) const = 0;
		bool virtual wczytaj(istream&) = 0;
	};
}