#include "TowarZKaucja.h"

namespace MySpace{
    string TowarZKaucja::opis() const{
        stringstream ss;
        ss<<kaucja;
        return "TowarZKaucja z kaucja: "+ss.str()+"\n";
    }
    bool TowarZKaucja::zapisz(ostream &os) const{
        os<<nazwa<<" ";
        wyprodukowano.zapisz(os);
        os<<" "<<cena<<" "<<vat<<" ";
        termin_zdatnosci.zapisz(os);
        os<<" "<<kaucja;
        return true;
    }
    bool TowarZKaucja::wczytaj(istream &is){
        int rok, miesiac, dzien;
        is>>nazwa>>rok>>miesiac>>dzien>>cena>>vat;
        wyprodukowano = Data(rok, miesiac, dzien);
        is>>rok>>miesiac>>dzien>>kaucja;
        termin_zdatnosci = Data(rok, miesiac, dzien);
        return true;
    }
}