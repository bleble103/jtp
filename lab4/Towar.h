#pragma once
#include "Produkt.h"
#include "Data.h"
#include<sstream>
namespace MySpace{
	class Towar : public Produkt{
	protected:
		double cena;
		double vat;
		Data termin_zdatnosci;

	public:
		Towar() = delete;
		Towar(const string &nazwa, const Data &wyprodukowano, double cena, double vat, const Data &termin_zdatnosci): Produkt(nazwa, wyprodukowano), cena(cena), vat(vat), termin_zdatnosci(termin_zdatnosci){
			cout<<"Towar contructor\n";
		}
		virtual ~Towar(){
			cout<<"Towar destructor\n";
		}
		string virtual opis() const;
		double virtual koszt() const{
			return cena*(1+vat);
		}
		bool virtual zapisz(ostream&) const;
		bool virtual wczytaj(istream&);
		Data pobierzTerminZdatnosci(){
			return termin_zdatnosci;
		}
	};
}