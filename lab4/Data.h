#pragma once
#include<iostream>
using namespace std;
namespace MySpace{
	
	class Data{
		int rok;
		int miesiac;
		int dzien;
	public:
		Data() = delete;
		Data(int rok, int miesiac, int dzien): rok(rok), miesiac(miesiac), dzien(dzien) {};
		void ustawRok(int newRok){
			rok = newRok;
		}
		int pobierzRok() const{
			return rok;
		}
		bool zapisz(ostream &os) const {
			return &(os<<rok<<" "<<miesiac<<" "<<dzien) != NULL;
		}
		bool wczytaj(istream &is){
			return &(is>>rok>>miesiac>>dzien) != NULL;
		}
		friend ostream& operator<<(ostream &os, const Data &d){
			return (d.zapisz(os))?os:os;
		}

	};
}