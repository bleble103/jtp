#pragma once

#include<iostream>
class Piksel{
    int x, y;

    public:
        Piksel(int x=0, int y=0) : x(x), y(y) {};
        void print(){
            std::cout<<"Piksel: "<<x<<", "<<y<<std::endl;
        }
};