#include "piksel.h"

class Poligon{
    Piksel *tab;
    int size;
    int index{0};

    public:
        Poligon(int size=10) : size(size){
            tab = new Piksel[size];
        }
        Poligon(Poligon&);
        Poligon(Poligon&&);

        void add(Piksel&);
        Piksel& get(int);

};