#include "poligon.h"

void Poligon::add (Piksel &p){
    if(index == size) throw "Full";
    tab[index++] = p;
}

Piksel& Poligon::get(int i){
    if(i<0 || i >= index) throw "Wrong index";
    return tab[i];
}
Poligon::Poligon(Poligon &cp){
    size = cp.size;
    index = cp.index;
    tab = new Piksel[size];
    for(int i=0; i<index; i++){
        tab[i] = cp.tab[i];
    }
}
Poligon::Poligon(Poligon &&mp){
    size = mp.size;
    index = mp.index;
    tab = mp.tab;
    mp.tab = nullptr;
    mp.size = 0;
    mp.index = 0;
}