#include<iostream>
#include "piksel.h"
#include "poligon.h"

using namespace std;

int main(){
    Piksel p1(2,3);
    p1.print();
    Poligon pol;
    pol.add(p1);
    pol.get(0).print();
    pol.get(1).print();
    return 0;
}