#include<iostream>
#include "punkt.h"
#include "kontener.h"

using namespace std;

int main(){
    Punkt p1(2,3);
    p1.wypisz();
    Kontener k(10);
    for(int i=0; i<7; i++){
        Punkt tmpP(i,i);
        k.add(tmpP);
    }
    cout<<"wszystkie:"<<endl;
    k.wypiszWszystkie();
    Kontener k2 = move(k);
    //k2.wypiszWszystkie();
    Punkt &wyjety = k2.get(3);
    wyjety.set(21, 37);
    cout<<endl;
    k2.wypiszWszystkie();
    cout<<"Length: "<<k2.length()<<endl;
    cout<<endl<<"original: "<<endl;
    k.wypiszWszystkie();
    Kontener k3(10);
    k3 = k2;
    k3.wypiszWszystkie();
    k3 = move(k2);
    k3.wypiszWszystkie();
    return 0;
}