#include "kontener.h"

Punkt& Kontener::add(Punkt &p){
    if(index == size){
        cout<<"Overfilled"<<endl;
        exit(1);
    }
    tab[index++] = p;
    return tab[index-1];
}

void Kontener::wypiszWszystkie(){
    for(int i=0; i<index; i++){
        tab[i].wypisz();
    }
}

Kontener::Kontener(Kontener &other){
    cout<<"Copy contructor"<<endl;
    size = other.size;
    tab = new Punkt[size];
    for(int i=0; i<size; i++){
        tab[i] = other.tab[i];
    }
    index = other.index;
}

Kontener::Kontener(Kontener &&other){
    cout<<"Move constructor"<<endl;
    this->tab = other.tab;
    this->size = other.size;
    this->index = other.index;
    other.tab = nullptr;
    other.size = 0;
    other.index = 0;
}

Punkt& Kontener::get(int i){
    if(i >= index){
        cout<<"Wrong index"<<endl;
        exit(1);
    }

    return tab[i];
}

Kontener& Kontener::operator= (Kontener &other){
    cout<<"Copy assignment"<<endl;
    if(this == &other){
        cout<<"same"<<endl;
    }else{
        delete [] tab;
        size = other.size;
        tab = new Punkt[size];
        for(int i=0; i<size; i++){
            tab[i] = other.tab[i];
        }
        index = other.index;
    }
}

Kontener& Kontener::operator= (Kontener &&other){
    cout<<"Move assignment"<<endl;
    if(this == &other){
        cout<<"same"<<endl;
    }else{
        delete [] tab;
        tab = other.tab;
        size = other.size;
        index = other.index;
        other.tab = nullptr;
        other.size = 0;
        other.index = 0;
    }
}