#pragma once
#include<iostream>

using namespace std;

class Punkt{
    int x, y;

    public:
        Punkt(int x=0, int y=0): x(x), y(y){};
        void wypisz(){
            cout<<"Punkt: "<<x<<", "<<y<<endl;
        }
        void set(int x, int y){
            this->x=x, this->y=y;
        }
};