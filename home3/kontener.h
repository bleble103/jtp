#pragma once
#include "punkt.h"

class Kontener{
    Punkt *tab;
    int size;
    int index{0};

    public:
        Kontener(int size=10) : size(size){
            tab = new Punkt[size];
        }
        Kontener(Kontener&);
        Kontener(Kontener&&);
        Kontener& operator=(Kontener&);
        Kontener& operator=(Kontener&&);
        ~Kontener(){
            delete [] tab;
        }
        Punkt &add(Punkt&);
        void wypiszWszystkie();

        Punkt &get(int);
        int length(){
            return index;
        }

};