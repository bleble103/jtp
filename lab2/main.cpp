#include<iostream>

using namespace std;

class Wektor;

class Macierz{
	double m00,m01,m10,m11;

	public:
		Macierz(double m00=0, double m01=0, double m10=0, double m11=0) : m00(m00), m01(m01), m10(m10), m11(m11) {};
		Wektor operator*(const Wektor&) const;
		Macierz operator-() const{
			return Macierz(-m00, -m01, -m10, -m11);
		}
		operator double() const{
			return m00*m11 - m01*m10;
		}

	friend ostream& operator<<(ostream&, Macierz);
	friend Macierz operator*(const Macierz&, double);
	friend Macierz operator*(double, const Macierz&);
};

class Wektor{
	double w0, w1;

	public:
		Wektor(double w0=0, double w1=0): w0(w0), w1(w1) {};
		Wektor operator+(const Wektor &w2) const{
			return Wektor(w0+w2.w0, w1+w2.w1);
		}
		Wektor operator+=(const Wektor &w){
			w0 += w.w0;
			w1 += w.w1;
			return *this;
		}
		Wektor operator++();
		Wektor operator++(int);
		bool operator==(const Wektor &w) const{
			return (w0 == w.w0) && (w1 == w.w1);
		}
		double operator[](int) const;


	friend ostream& operator<<(ostream&, Wektor);
	friend Wektor Macierz::operator*(const Wektor&) const;
};


double Wektor::operator[](int i) const{
	switch(i){
		case 0:
			return w0;
		case 1:
			return w1;
		default:
			exit(1);
	}
}

Wektor Wektor::operator++(int){
	Wektor tmp = *this;
	w0 += 1;
	w1 += 1;
	return tmp;
}

Wektor Wektor::operator++(){
	w0 += 1;
	w1 += 1;
	return *this;
}

Wektor Macierz::operator*(const Wektor &w) const{
	return Wektor(m00*w.w0 + m01*w.w1, m10*w.w0 + m11*w.w1);
}

Macierz operator*(const Macierz &m, double d){
	return Macierz(d*m.m00, d*m.m01, d*m.m10, d*m.m11);
}
Macierz operator*(double d, const Macierz &m){
	return Macierz(d*m.m00, d*m.m01, d*m.m10, d*m.m11);
}

ostream &operator<<(ostream &os, Macierz m) {
	os<<"Macierz:"<<endl<<"\t"<<m.m00<<"\t"<<m.m01<<endl<<"\t"<<m.m10<<"\t"<<m.m11;
	return os;
}
ostream &operator<<(ostream &os, Wektor w) {
	os<<"Wektor:"<<endl<<"\t"<<w.w0<<endl<<"\t"<<w.w1;
	return os;
}

int main(){
	Macierz m1(1,2,3,4);
	cout<<"Macierz m1(1,2,3,4);"<<endl;
	cout<<m1<<endl;

	Wektor w1(1,2);
	Wektor w2(2,3);
	cout<<endl<<"Wektor w1(1,2);"<<endl;
	cout<<"Wektor w1(2,3);"<<endl;
	cout<<w1<<endl;

	cout<<endl<<"(w1+w2)"<<endl;
	cout<<(w1+w2)<<endl;


	cout<<endl<<"(m1*2.0)"<<endl;
	Macierz mulsum = m1*2.0;
	cout<<mulsum<<endl;

	cout<<endl<<"Wektor mul = m1*w1;"<<endl;
	Wektor mul = m1*w1;
	cout<<mul<<endl;

	cout<<endl<<"mul++"<<endl;
	cout<<mul++<<endl;

	cout<<endl<<"mul"<<endl;
	cout<<mul<<endl;

	cout<<endl<<"++mul"<<endl;
	cout<<++mul<<endl;

	cout<<endl<<"m1"<<endl;
	cout<<m1<<endl;

	cout<<endl<<"-m1"<<endl;
	cout<<-m1<<endl;


	cout<<endl<<"(w1 == w2)"<<endl;
	cout<<(w1==w2)<<endl;
	Wektor w3(1,2);
	cout<<endl<<"Wektor w3(1,2); (w1 == w3)"<<endl;
	cout<<(w1==w3)<<endl;

	cout<<endl<<"w1[0] = "<<w1[0]<<", "<<"w1[1] = "<<w1[1]<<endl;

	cout<<endl<<"double detM1 = m1;"<<endl;
	double detM1 = m1;
	cout<<"detM = "<<detM1<<endl;

	/*
	Wektor ww;
	cout<<ww<<endl;
	Macierz mm;
	cout<<mm<<endl;
	*/

	return 0;
}
