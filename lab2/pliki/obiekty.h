#include<iostream>

using namespace std;

class Wektor;

class Macierz{
	double m00,m01,m10,m11;

	public:
		Macierz(double m00=0, double m01=0, double m10=0, double m11=0) : m00(m00), m01(m01), m10(m10), m11(m11) {};
		Wektor operator*(const Wektor&) const;
		Macierz operator-() const{
			return Macierz(-m00, -m01, -m10, -m11);
		}
		operator double() const{
			return m00*m11 - m01*m10;
		}

	friend ostream& operator<<(ostream&, Macierz);
	friend Macierz operator*(const Macierz&, double);
	friend Macierz operator*(double, const Macierz&);
};

class Wektor{
	double w0, w1;

	public:
		Wektor(double w0=0, double w1=0): w0(w0), w1(w1) {};
		Wektor operator+(const Wektor &w2) const{
			return Wektor(w0+w2.w0, w1+w2.w1);
		}
		Wektor& operator+=(const Wektor&);
		Wektor& operator++();
		Wektor operator++(int);
		bool operator==(const Wektor &w) const{
			return (w0 == w.w0) && (w1 == w.w1);
		}
		double operator[](int) const;


	friend ostream& operator<<(ostream&, Wektor);
	friend Wektor Macierz::operator*(const Wektor&) const;
};


Macierz operator*(const Macierz&, double);
Macierz operator*(double, const Macierz&);

ostream &operator<<(ostream&, Macierz);
ostream &operator<<(ostream&, Wektor);