#include<iostream>
#include "obiekty.h"

using namespace std;


int main(){
	Macierz m1(1,2,3,4);
	cout<<"Macierz m1(1,2,3,4);"<<endl;
	cout<<m1<<endl;

	Wektor w1(1,2);
	Wektor w2(2,3);
	cout<<endl<<"Wektor w1(1,2);"<<endl;
	cout<<"Wektor w1(2,3);"<<endl;
	cout<<w1<<endl;

	cout<<endl<<"(w1+w2)"<<endl;
	cout<<(w1+w2)<<endl;


	cout<<endl<<"(m1*2.0)"<<endl;
	Macierz mulsum = m1*2.0;
	cout<<mulsum<<endl;

	cout<<endl<<"Wektor mul = m1*w1;"<<endl;
	Wektor mul = m1*w1;
	cout<<mul<<endl;

	cout<<endl<<"mul++"<<endl;
	cout<<mul++<<endl;

	cout<<endl<<"mul"<<endl;
	cout<<mul<<endl;

	cout<<endl<<"++mul"<<endl;
	cout<<++mul<<endl;

	cout<<endl<<"m1"<<endl;
	cout<<m1<<endl;

	cout<<endl<<"-m1"<<endl;
	cout<<-m1<<endl;


	cout<<endl<<"(w1 == w2)"<<endl;
	cout<<(w1==w2)<<endl;
	Wektor w3(1,2);
	cout<<endl<<"Wektor w3(1,2); (w1 == w3)"<<endl;
	cout<<(w1==w3)<<endl;

	cout<<endl<<"w1[0] = "<<w1[0]<<", "<<"w1[1] = "<<w1[1]<<endl;

	cout<<endl<<"double detM1 = m1;"<<endl;
	double detM1 = m1;
	cout<<"detM = "<<detM1<<endl;

	/*
	Wektor ww;
	cout<<ww<<endl;
	Macierz mm;
	cout<<mm<<endl;
	*/

	return 0;
}
