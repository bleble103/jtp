#include "obiekty.h"


double Wektor::operator[](int i) const{
	switch(i){
		case 0:
			return w0;
		case 1:
			return w1;
		default:
			exit(1);
	}
    return 0;
}

Wektor Wektor::operator++(int){
	Wektor tmp = *this;
	w0 += 1;
	w1 += 1;
	return tmp;
}

Wektor& Wektor::operator++(){
	w0 += 1;
	w1 += 1;
	return *this;
}

Wektor& Wektor::operator+=(const Wektor &w){
    w0 += w.w0;
    w1 += w.w1;
    return *this;
}

Wektor Macierz::operator*(const Wektor &w) const{
	return Wektor(m00*w.w0 + m01*w.w1, m10*w.w0 + m11*w.w1);
}

Macierz operator*(const Macierz &m, double d){
	return Macierz(d*m.m00, d*m.m01, d*m.m10, d*m.m11);
}
Macierz operator*(double d, const Macierz &m){
	return Macierz(d*m.m00, d*m.m01, d*m.m10, d*m.m11);
}

ostream &operator<<(ostream &os, Macierz m) {
	os<<"Macierz:"<<endl<<"\t"<<m.m00<<"\t"<<m.m01<<endl<<"\t"<<m.m10<<"\t"<<m.m11;
	return os;
}
ostream &operator<<(ostream &os, Wektor w) {
	os<<"Wektor:"<<endl<<"\t"<<w.w0<<endl<<"\t"<<w.w1;
	return os;
}