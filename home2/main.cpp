#include<iostream>

using namespace std;
class Macierz;
class Punkt{
    double x1, x2;
    public:
        Punkt(double x1 = 0, double x2 = 0) : x1(x1), x2(x2) {};
        Punkt operator*(Macierz&);
        Punkt operator-(){
            return Punkt(-x1, -x2);
        }
        double operator[](int);
    
    friend ostream& operator<<(ostream&, Punkt);
};

class Macierz{
    double mxx, myy, mxy;
    public:
        double det(){
            return mxx*myy - mxy*mxy;
        }
        Macierz(double mxx=0, double myy=0, double mxy=0) : mxx(mxx), myy(myy), mxy(mxy){};
        Macierz& operator-=(Macierz&);
        Macierz operator-(Macierz&);
        Macierz& operator--();
        Macierz operator--(int);
        bool operator<(Macierz &m2){
            return this->det() < m2.det();
        }
        operator double(){
            return this->det();
        }

    friend ostream& operator<<(ostream&, Macierz);
    friend Macierz operator*(Macierz&, double);
    friend Macierz operator*(double, Macierz&);
    friend Punkt Punkt::operator*(Macierz&);
};

double Punkt::operator[](int i){
    switch(i){
        case 0:
            return x1;
        case 1:
            return x2;
        default:
            exit(1);
    }
}

Macierz& Macierz::operator--(){
    mxx--;
    myy--;
    mxy--;
    return *this;
}

Macierz Macierz::operator--(int){
    Macierz tmp = *this;
    mxx--;
    myy--;
    mxy--;
    return tmp;
}

Punkt Punkt::operator*(Macierz &m1){
    return Punkt(x1*m1.mxx + x2*m1.mxy, x1*m1.mxy + x2*m1.myy);
}

Macierz& Macierz::operator-=(Macierz &m2){
    mxx -= m2.mxx;
    myy -= m2.myy;
    mxy -= m2.mxy;
    return *this;
}

Macierz operator*(Macierz &m1, double d){
    return Macierz(m1.mxx*d, m1.myy*d, m1.mxy*d);
}
Macierz operator*( double d, Macierz &m1){
    return Macierz(m1.mxx*d, m1.myy*d, m1.mxy*d);
}

Macierz Macierz::operator-(Macierz &m2){
    return Macierz(mxx - m2.mxx, myy - m2.myy, mxy - m2.mxy);
}

ostream& operator<<(ostream &os, Punkt p1){
    os<<"Punkt: "<<p1.x1<<", "<<p1.x2;
    return os;
}
ostream& operator<<(ostream &os, Macierz m1){
    os<<"Macierz:"<<endl<<"\t"<<m1.mxx<<"\t"<<m1.mxy<<endl;
    os<<"\t"<<m1.mxy<<"\t"<<m1.myy;
    return os;
}

int main(){
    Punkt p1(2,3);
    cout<<p1<<endl<<p1<<endl;
    Macierz m1(1, 2, 3);
    cout<<m1<<endl;
    Macierz m2(1,1,1);
    //m1 -= m2;
    Macierz m3 = m1-m2;
    cout<<m3<<endl;
    cout<<(m1-m2)<<endl;
    cout<<m1*2.0<<endl;
    cout<<2.0*m1<<endl;
    cout<<p1*m1<<endl;
    cout<<m1--<<endl;
    cout<<m1<<endl;
    cout<<-p1<<endl;
    cout<<p1<<endl;
    cout<<m1<<endl;
    cout<<m2<<endl;
    cout<< (m1<m2) <<endl;
    cout<<p1[0]<<", "<<p1[1]<<endl;
    cout<<m1<<endl;
    cout<<(double)m1<<endl;
    return 0;
}