#include "poligon.h"

Piksel& Poligon::add(const Piksel &p){
    if(index >= size){
        throw "Overfilled";
    }
    tab[index++] = p;
    return tab[index-1];
}

void Poligon::wypiszWszystkie() const{
    for(int i=0; i<index; i++){
        tab[i].wypisz();
    }
}

Poligon::Poligon(const Poligon &other){
    cout<<"Copy contructor"<<endl;
    size = other.size;
    tab = new Piksel[size];
    for(int i=0; i<size; i++){
        tab[i] = other.tab[i];
    }
    index = other.index;
}

Poligon::Poligon(Poligon &&other){
    cout<<"Move constructor"<<endl;
    this->tab = other.tab;
    this->size = other.size;
    this->index = other.index;
    other.tab = nullptr;
    other.size = 0;
    other.index = 0;
}

Piksel& Poligon::get(int i) const{
    if(i >= index){
        throw "Wrong index";
    }
    return tab[i];
}

Poligon& Poligon::operator= (const Poligon &other){
    cout<<"Copy assignment"<<endl;
    if(this == &other){
        cout<<"same"<<endl;
    }else{
        delete [] tab;
        size = other.size;
        tab = new Piksel[size];
        for(int i=0; i<size; i++){
            tab[i] = other.tab[i];
        }
        index = other.index;
    }
    return *this;
}

Poligon& Poligon::operator= (Poligon &&other){
    cout<<"Move assignment"<<endl;
    if(this == &other){
        cout<<"same"<<endl;
    }else{
        delete [] tab;
        tab = other.tab;
        size = other.size;
        index = other.index;
        other.tab = nullptr;
        other.size = 0;
        other.index = 0;
    }
    return *this;
}