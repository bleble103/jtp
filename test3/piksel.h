#pragma once
#include<iostream>

using namespace std;

class Piksel{
    int x, y;

    public:
        Piksel(int x=0, int y=0): x(x), y(y){};
        void wypisz() const{
            cout<<"Piksel: "<<x<<", "<<y<<endl;
        }
        void set(int x, int y){
            this->x=x, this->y=y;
        }
};