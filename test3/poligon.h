#pragma once
#include "piksel.h"

class Poligon{
    Piksel *tab;
    int size;
    int index{0};

    public:
        Poligon(int size=10) : size(size){
            tab = new Piksel[size];
        }
        Poligon(const Poligon&);
        Poligon(Poligon&&);
        Poligon& operator=(const Poligon&);
        Poligon& operator=(Poligon&&);
        ~Poligon(){
            delete [] tab;
        }
        Piksel &add(const Piksel&);
        void wypiszWszystkie() const;

        Piksel &get(int) const;
        int length() const{
            return index;
        }
        int getSize() const{
            return size;
        }

};