#include<iostream>
#include "piksel.h"
#include "poligon.h"

using namespace std;

int main(){
    Piksel p1(2,3);
    p1.wypisz();
    Poligon k(10);
    try{
        for(int i=0; i<11; i++){ // przy i=10 wyjątek
            k.add(Piksel(i,i));
        }
    }catch(const char* msg){
        cout<<"**** Wyjatek: "<<msg<<" ****"<<endl;
    }
    
    cout<<"wszystkie:"<<endl;
    k.wypiszWszystkie();
    Poligon k2 = move(k);
    Piksel &wyjety = k2.get(3);
    wyjety.set(222, 333);
    cout<<endl;
    k2.wypiszWszystkie();
    cout<<"Length: "<<k2.length()<<endl;
    cout<<endl<<"original: "<<endl;
    k.wypiszWszystkie();
    Poligon k3(10);
    k3 = k2;
    k3.wypiszWszystkie();
    k3 = move(k2);
    k3.wypiszWszystkie();
    try{
        Piksel &tmpp = k3.get(10); //niepoprawny index, wyjątek 
        tmpp.wypisz();
    }
    catch(const char *msg){
        cout<<"**** Wyjątek: "<<msg<<" ****"<<endl;
    }
    Poligon copyPol = k3;
    copyPol = copyPol;
    copyPol = move(copyPol);
    cout<<"Liczba elementow w copyPol: "<<copyPol.length()<<endl<<endl;
    Poligon testPol(5);
    testPol.add(Piksel(2,2));
    testPol.add(Piksel(2,3));
    cout<<"Liczba elementow w testPol: "<<testPol.length()<<endl;
    cout<<"Maksymalny rozmiar w testPol: "<<testPol.getSize()<<endl;
    cout<<"Zawartosc testPol:"<<endl;
    testPol.wypiszWszystkie();

    cout<<"testPol = move(copyPol)"<<endl;
    testPol = move(copyPol);
    cout<<"Liczba elementow w testPol: "<<testPol.length()<<endl;
    cout<<"Maksymalny rozmiar w testPol: "<<testPol.getSize()<<endl;
    cout<<"Zawartosc testPol:"<<endl;
    testPol.wypiszWszystkie();
    return 0;
}