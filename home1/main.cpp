#include<iostream>
#include<iomanip>

using namespace std;

class Data{
    int d, m, r;
    static int lata;

    public:
    Data():d(1), m(1), r(2000) {
        lata += r;
    };
    Data(int, int, int);
    void wypisz(){
        cout<<setfill('0')<<setw(2)<<d<<'.'<<setw(2)<<m<<'.'<<setw(4)<<r<<endl;
    }
    void zapisz(ostream&);
    void wczytaj(istream&);
    int getYear(){
        return r;
    }
    void addYear(){
        r += 1;
    }
    bool isEarlier(Data&);
    static int getLata(){
        return lata;
    }
};
int Data::lata = 0;
bool Data::isEarlier (Data &other){
    if(r > other.r) return false;
    if(r == other.r){
        if(m > other.m) return false;
        if(m == other.m){
            return d < other.d;
        }
        return true;
    }
    return true;

}
void Data::wczytaj(istream &is){
    string inp;
    is>>inp;
    string tmp;
    int rok, mies, dzien;
    for(int j=0, i=0; j<3; j++){
        tmp = "";
        while(inp[i] != '-'){
            tmp += inp[i];
            i++;
        }
        i++;

        if(j == 0) rok = atoi(tmp.c_str());
        else if(j == 1) mies = atoi(tmp.c_str());
        else if(j == 2) dzien = atoi(tmp.c_str());
    }
    cout<<"rok: "<<rok<<" miesiac: "<<mies<<" dzien: "<<dzien<<endl;
}
void Data::zapisz(ostream &os){
    os << setw(4) << setfill('0') << r << '-';
    os << setw(2) << m << '-' << setw(2)<< d;
}
Data::Data(int _d, int _m, int _r = 2000){
    if(_m<1 or _m>12){
        d=1;
        m=1;
        r=2000;
    }
    else{
        d = _d;
        m = _m;
        r = _r;
    }
    lata += r;
}
int main(){
    cout<<"lata() = "<<Data::getLata()<<endl;
    Data data(1,2,2000);
    data.wczytaj(cin);
    cout<<"lata() = "<<Data::getLata()<<endl;
    Data data2;
    data.wypisz();
    data.zapisz(cout);
    cout<<"lata() = "<<Data::getLata()<<endl;
    cout<<"getYear() = "<<data.getYear()<<endl;
    // data.addYear();
    // cout<<"addYear() -> getYear() = "<<data.getYear()<<endl;
    cout<<"data:  ";
    data.wypisz();
    cout<<"data2: ";
    data2.wypisz();
    cout<<"data2 earlier than data: "<<data2.isEarlier(data)<<endl;
    return 0;
}