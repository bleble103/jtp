#include<iostream>
#include<vector>
#include<algorithm>
#include "drzewo.h"
#include<set>
#include<map>

using namespace std;

string wyszukiwane = "";

Drzewo generator(){
    static int index = 1;
    Drzewo d;
    d.setAge(index++);
    return d;
}

bool gatunek(Drzewo d){
    return d.isOfType(wyszukiwane);
}

void zwieksz(Drzewo &d){
    d.zwieksz();
}

bool usunWieksze(Drzewo &d){
    return d.czyWieksze(1.0);
}

bool mniejszaSrednica(const Drzewo &d1, const Drzewo &d2){
    return d1.getSrednica() < d2.getSrednica();
}

int main(){
    srand(time(NULL));


    Drzewo d;
    Drzewo d2;
    d.wypisz(cout);
    cout<<d.getKey()<<endl;
    d2.wypisz(cout);
    cout<<d2.getKey()<<endl;
    cout<<endl<<"d == d2: "<<(d==d2)<<endl;
    cout<<"d == d: "<<(d==d)<<endl;
    cout<<endl<<"d < d2: "<<(d<d2)<<endl;
    cout<<endl<<"d < d: "<<(d<d)<<endl;

    cout<<endl<<endl;

    vector<Drzewo> myvector(25);
    generate(myvector.begin(), myvector.end(), generator);
    vector<Drzewo>::iterator it = myvector.begin();
    while(it != myvector.end()){
        it->wypisz(cout);
        it++;
    }
    cout<<endl;

    cout<<"Najciensze drzewo:"<<endl;
    min_element(myvector.begin(), myvector.end(), mniejszaSrednica)->wypisz(cout);


    /*
    cout<<endl<<"Policz: ";
    cin>>wyszukiwane;
    cout<<"Znaleziono: "<< count_if(myvector.begin(), myvector.end(), gatunek)<<endl;
    */

    cout<<endl<<endl<<"Zwiekszono:"<<endl;
    for_each(myvector.begin(), myvector.end(), zwieksz);
    it = myvector.begin();
    while(it != myvector.end()){
        it->wypisz(cout);
        it++;
    }

    vector<Drzewo>::iterator newEnd = remove_if(myvector.begin(), myvector.end(), usunWieksze);
    cout<<endl<<endl<<"Usunieto wieksze niz 1m:"<<endl; 
    
    it = myvector.begin();
    while(it != newEnd){
        it->wypisz(cout);
        it++;
    }

    cout<<endl<<"Tworzenie zbioru:"<<endl;
    set<Drzewo> zbior;
    set<Drzewo>::iterator sit;
    pair<set<Drzewo>::iterator, bool> ret;
    it = myvector.begin();
    while(it != newEnd){
        ret = zbior.insert(*it);
        if(ret.second == false){
            cout<<"Element juz istnieje: ";
            it->wypisz(cout);
        }
        it++;
    }
    cout<<endl<<"Elementy zbioru:"<<endl;
    sit = zbior.begin();
    while(sit != zbior.end()){
        sit->wypisz(cout);
        sit++;
    }

    
    cout<<endl<<"Tworzenie mapy:"<<endl;
    map<int, Drzewo> mapa;
    map<int, Drzewo>::iterator mit;
    sit = zbior.begin();
    while(sit != zbior.end()){
        mapa[sit->getKey()] = *sit;
        //mapa.insert(pair<int, Drzewo>(sit->getKey(), *sit));
        sit++;
    }
    cout<<endl;
    mit=mapa.find(10);
    if(mit != mapa.end()){
        mit->second.wypisz(cout);
    }
    
    

    return 0;
}