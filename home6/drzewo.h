#pragma once

#define GAT_NO 5

using namespace std;

static string gatunki[GAT_NO] = {"dab", "orzech", "klon", "wierzba", "sosna"};

class Drzewo{
    string gatunek;
    double srednica;
    int wiek;

    public:
    Drzewo();
    ostream &wypisz(ostream &os) const{
        return os<<"Drzewo: "<<this->gatunek<<", d: "<<this->srednica<<", wiek: "<<this->wiek<<endl;
    }
    int getKey() const{
        return this->gatunek.length()+this->wiek;
    }
    bool operator==(Drzewo &other){
        return this->getKey()==other.getKey();
    }
    bool operator<(const Drzewo &other) const{
        return this->getKey()<other.getKey();
    }
    void setAge(int age){
        this->wiek = age;
    }
    bool isOfType(string which){
        return which==this->gatunek;
    }
    void zwieksz(){
        this->srednica += 0.1;
    }
    bool czyWieksze(double od){
        return this->srednica > od;
    }
    double getSrednica() const{
        return this->srednica;
    }
};

Drzewo::Drzewo() {
    this->gatunek = gatunki[rand()%GAT_NO];
    this->srednica = (rand()%20 + 1)/10.0;
    this->wiek = rand()%100 + 1;
}