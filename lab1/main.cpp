#include<iostream>
#include<iomanip>
#include<cmath>

using namespace std;

class Punkt{
    float x, y, z;
    static int licznik;

    public:
        Punkt(): x(0), y(0), z(0){
            licznik++;
        };
        Punkt(float cx, float cy, float cz = 0) : x(cx), y(cy), z(cz) {
            licznik++;
        };
        float getX(){
            return x;
        }
        float setX(float sx){
            x = sx;    
        }
        float dist(Punkt &other){
            return sqrt(pow(x-other.x, 2) + pow(y-other.y, 2) + pow(z-other.z, 2));
        }
        void wypisz(){
            cout<<x<<", "<<y<<", "<<z<<endl;
        }
        void zapisz(ostream&);
        void wczytaj(istream&);

        static int getLicznik(){
            return licznik;
        }
};

void Punkt::zapisz(ostream &os){
    os<<fixed<<setprecision(4);
    os<<"["<<x<<", "<<y<<", "<<z<<"]"<<endl;
    /*
    char buff[50];
    snprintf(buff, 50, "[%.4f, %.4f, %.4f]", x, y, z);
    string output = buff;
    os << output;
    */
}

void Punkt::wczytaj(istream &is){
    string tmp;
    is.ignore(1);
    is >> x;
    is.ignore(2);
    is >> y;
    is.ignore(2);
    is >> z;
    is.ignore(1e7, '\n');

    /*
    string input = "";
    string tmp;

    is>>input;
    is>>tmp;
    input += " " + tmp;
    is>>tmp;
    input += " " + tmp;

    
    sscanf(input.c_str(), "[%f, %f, %f]", &x, &y, &z);
    */
}

int Punkt::licznik = 0;

int main(){
    
    cout<<"Licznik: "<<Punkt::getLicznik()<<endl;
    Punkt p;
    cout<<"Licznik: "<<Punkt::getLicznik()<<endl;
    cout<<p.getX()<<endl;
    p.setX(2.0f);
    cout<<p.getX()<<endl;

    Punkt p2(0,0,0);
    cout<<"Licznik: "<<p2.getLicznik()<<endl;
    cout<<"Dist: "<<p.dist(p2)<<endl;
    cout<<"p: ";
    p.wypisz();
    cout<<"p2: ";
    p2.wypisz();

    cout<<"p.zapisz(): ";
    p.zapisz(cout);
    cout<<endl;
    cout<<"p2.zapisz(): ";
    p2.zapisz(cout);
    cout<<endl;

    cout<<"wczytaj p1: ";
    p.wczytaj(cin);

    cout<<"wczytano: ";
    p.zapisz(cout);
    cout<<endl;

    cout<<"wczytaj p1: ";
    p.wczytaj(cin);

    cout<<"wczytano: ";
    p.zapisz(cout);
    cout<<endl;
    return 0;
}